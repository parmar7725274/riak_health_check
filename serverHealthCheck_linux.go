//File contains function to get Health status for Linux platform

package main

import ( 
       //   "fmt"
					"strings"
					"bufio"
					"bytes"
					"io"
					"io/ioutil"
					"strconv"
					"syscall"
)

/*
		Function gives CPU health Status
*/
func Get_cpu_health_status( status *Health_check_result ) error {
	
	var user, nice, system, idle, iowait, irq, softirq, steal uint64
	
	handler	:=	func(line string) bool {
								if len(line) > 3 && line[0:3] == "cpu" && line[3] == ' ' {
									
									fields := strings.Fields(line)
									
									user, _		=	strconv.ParseUint(fields[1], 10, 64)
									nice, _		=	strconv.ParseUint(fields[2], 10, 64)	
									system, _	=	strconv.ParseUint(fields[3], 10, 64)
									idle, _		=	strconv.ParseUint(fields[4], 10, 64)
									iowait, _	=	strconv.ParseUint(fields[5], 10, 64)
									irq, _		=	strconv.ParseUint(fields[6], 10, 64)
									softirq, _=	strconv.ParseUint(fields[7], 10, 64)
									steal, _	=	strconv.ParseUint(fields[8], 10, 64)
								}
								return true
							}

	if err := readFile("/proc/stat", handler ); err != nil {
		return err
  }
	
	total_non_idle	:=	user + nice + system + irq + softirq + steal
	total_idle	:=	idle + iowait
	
	total	:= total_non_idle + total_idle
	
	percent_utilize := float64( total_non_idle * 100 )/float64(total)
	
	//fmt.Printf("\n\nCpu Utilization : %v\n", percent_utilize )
	
	status.Name	=	"Server CPU Health Check."
	
	if percent_utilize >= Cpu_utilization_warning_threshold {
		status.Status	= "warning"	
	} else {
		status.Status	= "healthy"
	}
	
	status.Description	=	"CPU Status : Total :"+strconv.FormatUint(total, 10)+", Total non Idle : "+strconv.FormatUint(total_non_idle, 10)+", Total idle :"+strconv.FormatUint(total_idle, 10)+" Utilize(%) : "+strconv.FormatFloat( percent_utilize, 'f', -1, 32)
	
	return nil
}

/*
		Function gives Disk health status
*/
func Get_disk_health_status( status *Health_check_result ) error {
	
	var total_disk_size, free_disk_space, used_disk_space uint64
	
	fs := syscall.Statfs_t{}
	
	if err := syscall.Statfs("/", &fs); err != nil {
		return err
	}
	
	total_disk_size = (fs.Blocks * uint64(fs.Bsize))/2048
	free_disk_space = (fs.Bfree * uint64(fs.Bsize))/2048
	used_disk_space = total_disk_size - free_disk_space
	
	percent_utilize	:= float64( used_disk_space * 100 )/float64(total_disk_size)
	
	status.Name	=	"Server Disk usage health Check."
	
	if percent_utilize >= Disk_usage_health_status {
		status.Status	= "warning"	
	} else {
		status.Status	= "healthy"
	}
	
	status.Description	=	"Mounted on : '/' . Disk Size : "+strconv.FormatUint(total_disk_size, 10)+" GB, Used : "+strconv.FormatUint(used_disk_space,10)+" GB, Utilize(%) : "+strconv.FormatFloat( percent_utilize, 'f', -1, 32)
	
  return nil
}
/*
		Function gives Main Memory health Status
*/
func Get_memory_health_status( status *Health_check_result ) error {
  
  var total_memory, free_memory uint64
  
  table := map[string]*uint64{
                                "MemTotal": &total_memory,
                                "MemFree" : &free_memory,
                              }
  
  if err := parseMeminfo(table); err != nil {
		return err
  }
  
	used_memory	:= total_memory - free_memory
	
	percent_utilize := float64( used_memory * 100 )/float64(total_memory)
	
	//fmt.Printf( "\nMemory Total : %v \t Free : %v\t used : %v\t Percentage utilize : %v", 
	//					 											total_memory, free_memory, used_memory, percent_utilize )
  
	status.Name	=	"Server Main Memory Health Check."
	
	if percent_utilize >= Memory_usage_warning_threshold {
		status.Status	= "warning"	
	} else {
		status.Status	= "healthy"
	}
	
	status.Description	=	"Total Memory : "+strconv.FormatUint(total_memory, 10)+" kB, Used Memory : "+strconv.FormatUint(used_memory,10)+" kB, Utilize(%) : "+strconv.FormatFloat( percent_utilize, 'f', -1, 32)
	
  return nil
}


/*
		Function gives Swap Memory health Status
*/
func Get_swap_health_status( status *Health_check_result ) error {
  
  var total_memory, free_memory uint64
  
  table := map[string]*uint64{
                                "SwapTotal": &total_memory,
                                "SwapFree" : &free_memory,
                              }
  
  if err := parseMeminfo(table); err != nil {
		return err
  }
  
	used_memory	:= total_memory - free_memory
	
	percent_utilize := float64( used_memory * 100 )/float64(total_memory)
	
	//fmt.Printf( "\nSwap Total : %v \t Free : %v\t used : %v\t Percentage utilize : %v", 
	//					 											total_memory, free_memory, used_memory, percent_utilize )
  
	status.Name	=	"Server Swap Memory Health Check."
	
	if percent_utilize >= Swap_memory_usage_warning_threshold {
		status.Status	= "warning"	
	} else {
		status.Status	= "healthy"
	}
	
	status.Description	=	"Total Memory : "+strconv.FormatUint(total_memory, 10)+" kB, Used Memory : "+strconv.FormatUint(used_memory,10)+" kB, Utilize(%) : "+strconv.FormatFloat( percent_utilize, 'f', -1, 32)
	
  return nil
} 

/*

*/
func parseMeminfo(table map[string]*uint64) error {
  
  handler :=  func(line string) bool {
                                        fields := strings.Split(line, ":")

                                        if ptr := table[fields[0]]; ptr != nil {
                                          
                                          num := strings.TrimLeft(fields[1], " ")
                                          
                                          val, err := strconv.ParseUint( strings.Fields(num)[0], 10, 64) 
                                          
                                          if err == nil {
                                            *ptr = val 
                                          }
                                        }

                                        return true
                                      }
  
	return readFile("/proc/meminfo", handler )
  
}

/*

*/
func readFile(file string, handler func(string) bool) error {
  
	contents, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	reader := bufio.NewReader(bytes.NewBuffer(contents))

	for {
    
		line, _, err := reader.ReadLine()
    
		if err == io.EOF {
			break
		}
    
		if !handler(string(line)) {
			break
		}
    
	}

	return nil
}